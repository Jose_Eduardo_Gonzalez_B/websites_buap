function verificar_vacio(){ //a)
  var mensaje = "El campo: ";
  if(document.getElementById("form-name").value == ''){
    mensaje += "nombre, ";
  }
  if(document.getElementById("form-email").value == ''){
    mensaje += "email, ";
  }
  if(document.getElementById("form-tel").value == ''){
    mensaje += "telefono, ";
  }
  mensaje += "esta vacio";
  if(mensaje != "El campo: esta vacio"){
    alert(mensaje);
  }
}
function verificar_email(){ //b)
  var mail = document.getElementById("form-email").value;
  var car = 'qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNM1234567890';
  mensaje = '';
  var bandera = 0;
  for(var i = 0; i < car.length; i++){
    if(mail.charAt(0) == car.charAt(i)){
      bandera++;
    }
  }
  if(bandera == 0){
    alert("El email debe comenzar con una letra o un numero");
  }
  if(mail.indexOf('.net') != -1 || mail.indexOf('.com') != -1 || mail.indexOf('.mx') != -1 || mail.indexOf('.org') != -1 ){
  }else{
    alert("El email debe terminar con un .net, .com, .mx, .org");
  }
}

function verificar_telefono(){ //c)
  var num = document.getElementById("form-tel").value;
  if(num.length != 10){
    alert("El numero Telefonico debe tener 10 caracteres exactamente");
  }
}
function manejador(myRadio,col){ //d)
  alert('Nuevo valor: ' + myRadio.value);
  document.getElementById("color").style.background = myRadio.value;
  document.getElementById("color").style.color = col;
}

function talla(){ //e)
  var usa = parseInt(document.getElementById('size_zapato').value);
  var mex = 0;
  if(usa == 5){
    mex = 23;
  }
  if(usa == 6){
    mex = 24;
  }
  if(usa == 7){
    mex = 25;
  }
  if(usa == 8){
    mex = 26;
  }
  if(usa == 9){
    mex = 27;
  }
  if(usa == 10){
    mex = 28;
  }
  if(usa == 11){
    mex = 29;
  }
  if(usa == 12){
    mex = 30;
  }
  if(usa == 13){
    mex = 31;
  }
  alert("La talla " + usa + " norteamericana equivale a una talla " + mex + " en México");
}

function textarea_max(){
  text = document.getElementById('form-story').value;
  if(text.length > 300){
    alert("La historia no debe exeder los 300 caracteres");
  }
}

function verificar_caracteristicas(){
  var e = 0;
  if(document.getElementById('car_1').checked){
    e++;
  }
  if(document.getElementById('car_2').checked){
    e++;
  }
  if(document.getElementById('car_3').checked){
    e++;
  }
  if(document.getElementById('car_4').checked){
    e++;
  }
  if(e == 0){
    alert("Se debe seleccionar almenos una caracteristica");
  }
}

function todo(){
  verificar_vacio();
  textarea_max();
  verificar_caracteristicas();
}
