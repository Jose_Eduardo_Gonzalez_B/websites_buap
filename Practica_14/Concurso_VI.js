function verificar_nombre(){
  var mensaje = "";
  var color_fondo;
  var color_text;
  if(document.getElementById("form-name").value == ''){
    mensaje = "El campo nombre, esta vacio";
    color_fondo = "#ff0000";
    color_text = "#ffffff";
  }else{
    mensaje = "";
    color_fondo = "#ffffff";
    color_text = "#000000";
  }
  var iframe = document.getElementById('name');
  var html_string = '<html><head></head><body bgcolor="'+color_fondo+'" text="'+color_text+'"><p>'+mensaje+'</p></body></html>';
  var iframedoc = iframe.document;
  if (iframe.contentDocument)
      iframedoc = iframe.contentDocument;
  else
      if(iframe.contentWindow)
      iframedoc = iframe.contentWindow.document;
  if(iframedoc){
      iframedoc.open();
      iframedoc.writeln(html_string);
      iframedoc.close();
  }
}

function verificar_email(){
  var mensaje = "";
  var color_fondo;
  var color_text;
  if(document.getElementById("form-email").value == ''){
    mensaje = "El campo email, esta vacio";
    color_fondo = "#ff0000";
    color_text = "#ffffff";
  }else{
    mensaje = "";
    color_fondo = "#ffffff";
    color_text = "#000000";
  }
  var mail = document.getElementById("form-email").value;
  if(mail.indexOf('.net') != -1 || mail.indexOf('.com') != -1 || mail.indexOf('.mx') != -1 || mail.indexOf('.org') != -1 ){

  }else{
    mensaje += " Debe terminar en .net, .com, .mx, .org";
    color_fondo = "#ff0000";
    color_text = "#ffffff";
  }
  var iframe = document.getElementById('email');
  var html_string = '<html><head></head><body bgcolor="'+color_fondo+'" text="'+color_text+'"><p>'+mensaje+'</p></body></html>';
  var iframedoc = iframe.document;
  if (iframe.contentDocument)
      iframedoc = iframe.contentDocument;
  else
      if(iframe.contentWindow)
      iframedoc = iframe.contentWindow.document;
  if(iframedoc){
      iframedoc.open();
      iframedoc.writeln(html_string);
      iframedoc.close();
  }
}
function verificar_telefono(){ //c)
  var mensaje = "";
  var color_fondo = "#ffffff";
  var color_text = "#000000";
  var num = document.getElementById("form-tel").value;
  var car = 'qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNM1234567890';
  var mail = document.getElementById("form-email").value;
  var bandera = 0;
  if(document.getElementById("form-tel").value == ''){
    mensaje = "El campo Telefónico, esta vacio";
    color_fondo = "#ff0000";
    color_text = "#ffffff";
  }else{
    if(num.length != 10){
      mensaje = "El numero Telefonico debe tener 10 caracteres exactamente";
      color_fondo = "#ff0000";
      color_text = "#ffffff";
    }
    for(var i = 0; i < car.length; i++){
      if(mail.charAt(0) == car.charAt(i)){
        bandera++;
      }
    }
    if(bandera == 0){
      mensaje="El email debe comenzar con una letra o un numero";
    }
  }
  var iframe = document.getElementById('tel');
  var html_string = '<html><head></head><body bgcolor="'+color_fondo+'" text="'+color_text+'"><p>'+mensaje+'</p></body></html>';
  var iframedoc = iframe.document;
  if (iframe.contentDocument)
      iframedoc = iframe.contentDocument;
  else
      if(iframe.contentWindow)
      iframedoc = iframe.contentWindow.document;
  if(iframedoc){
      iframedoc.open();
      iframedoc.writeln(html_string);
      iframedoc.close();
  }
}
function manejador(myRadio,col){ //d)
  document.getElementById("color").style.background = myRadio.value;
  document.getElementById("color").style.color = col;
}

function talla(){ //e)
  var usa = parseInt(document.getElementById('size_zapato').value);
  var mex = 0;
  var color_fondo = "#00ff00";
  var color_text = "#000000";
  if(usa == 5){
    mex = 23;
  }
  if(usa == 6){
    mex = 24;
  }
  if(usa == 7){
    mex = 25;
  }
  if(usa == 8){
    mex = 26;
  }
  if(usa == 9){
    mex = 27;
  }
  if(usa == 10){
    mex = 28;
  }
  if(usa == 11){
    mex = 29;
  }
  if(usa == 12){
    mex = 30;
  }
  if(usa == 13){
    mex = 31;
  }
  mensaje = "La talla " + usa + " norteamericana equivale a una talla " + mex + " en México";
  var iframe = document.getElementById('zapatos');
  var html_string = '<html><head></head><body bgcolor="'+color_fondo+'" text="'+color_text+'"><p>'+mensaje+'</p></body></html>';
  var iframedoc = iframe.document;
  if (iframe.contentDocument)
      iframedoc = iframe.contentDocument;
  else
      if(iframe.contentWindow)
      iframedoc = iframe.contentWindow.document;
  if(iframedoc){
      iframedoc.open();
      iframedoc.writeln(html_string);
      iframedoc.close();
  }
}

function textarea_max(){
  var text = document.getElementById('form-story').value;
  var mensaje = "";
  var color_fondo = "#ffffff";
  var color_text = "#000000";
  if(text.length > 300){
    mensaje = "La historia no debe exeder los 300 caracteres";
    color_fondo = "#ff0000";
    color_text = "#ffffff";
  }
  var iframe = document.getElementById('story_1');
  var html_string = '<html><head></head><body bgcolor="'+color_fondo+'" text="'+color_text+'"><p>'+mensaje+'</p></body></html>';
  var iframedoc = iframe.document;
  if (iframe.contentDocument)
      iframedoc = iframe.contentDocument;
  else
      if(iframe.contentWindow)
      iframedoc = iframe.contentWindow.document;
  if(iframedoc){
      iframedoc.open();
      iframedoc.writeln(html_string);
      iframedoc.close();
  }
}

function verificar_caracteristicas(){
  var e = 0;
  var mensaje = "";
  var color_fondo = "#ffffff";
  var color_text = "#000000";
  if(document.getElementById('car_1').checked){
    e++;
  }
  if(document.getElementById('car_2').checked){
    e++;
  }
  if(document.getElementById('car_3').checked){
    e++;
  }
  if(document.getElementById('car_4').checked){
    e++;
  }
  if(e == 0){
    mensaje = "Se debe seleccionar almenos una caracteristica";
    color_fondo = "#ff0000";
    color_text = "#ffffff";
  }
  var iframe = document.getElementById('car');
  var html_string = '<html><head></head><body bgcolor="'+color_fondo+'" text="'+color_text+'"><p>'+mensaje+'</p></body></html>';
  var iframedoc = iframe.document;
  if (iframe.contentDocument)
      iframedoc = iframe.contentDocument;
  else
      if(iframe.contentWindow)
      iframedoc = iframe.contentWindow.document;
  if(iframedoc){
      iframedoc.open();
      iframedoc.writeln(html_string);
      iframedoc.close();
  }
}
